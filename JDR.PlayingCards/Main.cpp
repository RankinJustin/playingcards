
// Playing Cards
// Justin Rankin

#include <iostream>
#include <conio.h>

using namespace std;

enum class Suit
{
	Hearts,
	Diamonds,
	Spades,
	Clubs
};
enum class Rank 
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

int main()
{
	Card c1;
	c1.Rank = Rank::Ace;
	c1.Suit = Suit::Spades;

	(void)_getch();
	return 0;
}

